﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemAchievementTracker : MonoBehaviour {

	public static GemAchievementTracker Instance;
    public GameObject CanvasGameObject;
    public GameObject AchievementUnlockedPanelGameObject;
    public int RequiredGems = 30;
    public int GemsAcquired = 0;

    public string AchievementName;
    public string AchievementText;
    public int IsAchieved = 0;

    void Start()
    {
        Instance = this;
		// Debugger
		// PlayerPrefs.SetInt(AchievementName + "Achieved", 0);

        IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);
        GemsAcquired = 0;
    }

    // Update is called once per frame
    public void IncreaseGemsAcquired()
    {
        GemsAcquired += 1;

        if (RequiredGems == GemsAcquired && IsAchieved == 0)
        {
            PlayerPrefs.SetInt(AchievementName + "Achieved", 1);
            IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);

            TriggerAchiementToast();
        }
    }

    private void TriggerAchiementToast()
    {
        var achievementUnlockedPanelGameObject = Instantiate(
            AchievementUnlockedPanelGameObject,
            CanvasGameObject.transform
        );

        achievementUnlockedPanelGameObject.GetComponent<AchievementUnlockedPanel>()
            .AchievementText = AchievementText;
    }
}
