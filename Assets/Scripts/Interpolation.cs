class Interpolation
{
    public static float EaseOut(float u)
    {
        return u < 0.5 ? 2 * u * u : -1 + (4 - 2 * u) * u;
    }
}