﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AwardsButton : MonoBehaviour {

	public void OnAwardsButtonClicked()
	{
		SceneManager.LoadScene("AchievementScene");
	}
}
