﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmationPanel : MonoBehaviour {
    public static ConfirmationPanel Instance;
    public int AffectedUpgradeBoxIndex;
    public int AffectedUpgradeBoxCost;

	// Use this for initialization
	void Start () {
		if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(gameObject);
        }

		Hide();
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
