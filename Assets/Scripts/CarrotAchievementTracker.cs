﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarrotAchievementTracker : MonoBehaviour
{
    public static CarrotAchievementTracker Instance;
    public GameObject CanvasGameObject;
    public GameObject AchievementUnlockedPanelGameObject;
    public int RequiredCarrots = 30;
    public int CarrotsAcquired = 0;

    public string AchievementName;
    public string AchievementText;
    public int IsAchieved = 0;

    void Start()
    {
        Instance = this;
		// Debugger
		// PlayerPrefs.SetInt(AchievementName + "Achieved", 0);

        IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);
        CarrotsAcquired = 0;
    }

    // Update is called once per frame
    public void IncreaseCarrotsAcquired()
    {
        CarrotsAcquired += 1;

        if (RequiredCarrots == CarrotsAcquired && IsAchieved == 0)
        {
            PlayerPrefs.SetInt(AchievementName + "Achieved", 1);
            IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);

            TriggerAchiementToast();
        }
    }

    private void TriggerAchiementToast()
    {
        var achievementUnlockedPanelGameObject = Instantiate(
            AchievementUnlockedPanelGameObject,
            CanvasGameObject.transform
        );

        achievementUnlockedPanelGameObject.GetComponent<AchievementUnlockedPanel>()
            .AchievementText = AchievementText;
    }
}
