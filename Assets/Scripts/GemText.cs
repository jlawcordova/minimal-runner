﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemText : MonoBehaviour {

	public Text Text;

	// Use this for initialization
	void Start () {
		Text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		Text.text = GemController.Instance.Gem.ToString();
	}
}
