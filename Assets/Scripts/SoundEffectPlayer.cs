﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectPlayer : MonoBehaviour {

	public static SoundEffectPlayer Instance;

	public AudioClip HealthyFoodAudioClip;
	public AudioClip JunkFoodAudioClip;
	public AudioClip GemAudioClip;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		Instance = this;
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	public void PlayHealthyFood()
	{
		audioSource.clip = HealthyFoodAudioClip;
		audioSource.Play();
	}

	public void PlayGem()
	{
		audioSource.clip = GemAudioClip;
		audioSource.Play();
	}

	public void PlayDeath()
	{
		audioSource.clip = JunkFoodAudioClip;
		audioSource.Play();
	}
}
