﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    // Gameobject where the camera focuses
    public GameObject GameObjectOfInterest;

    private Vector3 positionOfInterest;


    // Update is called once per frame
    void LateUpdate()
    {
        FocusOnPointOfInterest();
    }

    private void FocusOnPointOfInterest()
    {
        positionOfInterest = GameObjectOfInterest.transform.position;

        Vector3 position = positionOfInterest;
        position.y += 4;
        position.z += -7;

        transform.position = position;
    }
}
