﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAchievementTracker : MonoBehaviour {

	public static ScoreAchievementTracker Instance;
    public GameObject CanvasGameObject;
    public GameObject AchievementUnlockedPanelGameObject;
    public int RequiredScore = 50;

    public string AchievementName;
    public string AchievementText;
    public int IsAchieved = 0;

    void Start()
    {
        Instance = this;
		// Debugger
		// PlayerPrefs.SetInt(AchievementName + "Achieved", 0);

        IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);
    }

    // Update is called once per frame
    public void CheckScoreAchievement(int scoreAcquired)
    {
        if (RequiredScore == scoreAcquired && IsAchieved == 0)
        {
            PlayerPrefs.SetInt(AchievementName + "Achieved", 1);
            IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);

            TriggerAchiementToast();
        }
    }

    private void TriggerAchiementToast()
    {
        var achievementUnlockedPanelGameObject = Instantiate(
            AchievementUnlockedPanelGameObject,
            CanvasGameObject.transform
        );

        achievementUnlockedPanelGameObject.GetComponent<AchievementUnlockedPanel>()
            .AchievementText = AchievementText;
    }
}
