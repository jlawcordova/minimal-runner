﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmationButton : MonoBehaviour {

	// Use this for initialization
	public void OnYesClicked()
	{
		UpgradeController.Instance.UnlockUpgrade(
			ConfirmationPanel.Instance.AffectedUpgradeBoxIndex
		);
		GemController.Instance.DecreaseGem(ConfirmationPanel.Instance.AffectedUpgradeBoxCost);
		ConfirmationPanel.Instance.Hide();
	}

	public void OnNoClicked()
	{
		ConfirmationPanel.Instance.Hide();
	}
}
