﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadEndpoint : MonoBehaviour
{
    private Road road;
    // Use this for initialization
    void Start()
    {
        road = transform.parent.GetComponent<Road>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "RoadOutCycler")
        {
            road.CycleOut();
        }
    }
}
