﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeController : MonoBehaviour {

	public static UpgradeController Instance;
    public bool[] UpgradeUnlocked = new bool[4];
	// Use this for initialization
	void Start () {
		if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(gameObject);
        }

		DontDestroyOnLoad(gameObject);
		PlayerPrefs.SetInt("UpgradeUnlocked0", 1);
		UpgradeUnlocked[0] = PlayerPrefs.GetInt("UpgradeUnlocked0", 1) == 0 ? false : true;
		for (int i = 0; i < UpgradeUnlocked.Length; i++)
		{
        	UpgradeUnlocked[i] = PlayerPrefs.GetInt("UpgradeUnlocked" + i.ToString(), 0) == 0 ? false : true;
		}
	}
	
	// Update is called once per frame
	public void UnlockUpgrade(int upgrade)
    {
		UpgradeUnlocked[upgrade] = upgrade == 0 ? false : true;
		PlayerPrefs.SetInt("UpgradeUnlocked" + upgrade.ToString(), 1);
    }
}
