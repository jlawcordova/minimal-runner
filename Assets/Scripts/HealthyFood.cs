﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthyFood : MonoBehaviour
{
    public enum HealthyFoodType
    {
        Carrot,
        Chicken,
        Milk
    }

    public LayerMask HeroLayer;
    public GameObject HealthyFoodContainer;
    private Animator healthyFoodContainerAnimator;
    public HealthyFoodType Type = HealthyFoodType.Chicken;
    private bool scored = false;

    // Use this for initialization
    void Start()
    {
        healthyFoodContainerAnimator = HealthyFoodContainer.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        var hit = Physics.BoxCast(transform.position, new Vector3(0.21f, 1f, 0.21f),
            Vector3.up, Quaternion.identity, Mathf.Infinity, HeroLayer, QueryTriggerInteraction.Collide);

        if (hit && !scored)
        {
            healthyFoodContainerAnimator.SetTrigger("shouldFly");
            scored = true;
            GameManager.Instance.IncreaseScore(1);
            SoundEffectPlayer.Instance.PlayHealthyFood();
            switch (Type)
            {
                case HealthyFoodType.Carrot:
                    CarrotAchievementTracker.Instance.IncreaseCarrotsAcquired();
                    break;
                default:
                    break;
            }
            Invoke("DelayedDestory", 0.5f);
        }
    }

    public void DelayedDestory()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "RoadOutCycler")
        {
            Destroy(gameObject);
        }
    }
    // void OnDrawGizmos() {
    // 	Gizmos.DrawWireCube(transform.position, new Vector3(0.5f, 20f, 0.5f));
    // }
}
