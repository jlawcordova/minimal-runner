﻿using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;

public class LoginButton : MonoBehaviour {

	public GameObject UsernameGameObject;
	public GameObject PasswordGameObject;
	public GameObject InvalidCredentialsGameObject;

	private InputField Username;
	private InputField Password;

	// Use this for initialization
	void Start () {
		InvalidCredentialsGameObject.SetActive(false);
		Username = UsernameGameObject.GetComponent<InputField>();
		Password = PasswordGameObject.GetComponent<InputField>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoginClicked()
	{
		if (Login(Username.text, Password.text))
		{
			StartGame();
		}
		else 
		{
			ShowInvalidCredentials();
		}
	}

    private void ShowInvalidCredentials()
    {
		InvalidCredentialsGameObject.SetActive(true);
    }

    private void StartGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    private bool Login(string username, string password)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://13.250.11.43:8100/api/get-student-data.php?u={0}&p={1}", username, password));
		HttpWebResponse response = (HttpWebResponse)request.GetResponse();
		StreamReader reader = new StreamReader(response.GetResponseStream());
		string jsonResponse = reader.ReadToEnd();
		jsonResponse = "{\"values\":" + jsonResponse + "}";
		StudentWrapper info = JsonUtility.FromJson<StudentWrapper>(jsonResponse);
		if (info.values[0].studentid == null)
		{
			return false;
		}
		else
		{
			IntegrationManager.Instance.LoggedInStudent = info.values[0];
			return true;
		}
    }
}
