﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedHeroController : MonoBehaviour
{
    public static SelectedHeroController Instance;
    public int SelectedHero;

    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        SelectedHero = PlayerPrefs.GetInt("SelectedHero", 0);
    }

    // Update is called once per frame
    public void SetSelectedHero(int selectedHero)
    {
        SelectedHero = selectedHero;
        PlayerPrefs.SetInt("SelectedHero", selectedHero);
    }
}
