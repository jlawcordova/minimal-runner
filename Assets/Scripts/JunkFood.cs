﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JunkFood : MonoBehaviour
{

    public LayerMask HeroLayer;

    // Update is called once per frame
    void Update()
    {
        var hit = Physics.BoxCast(transform.position, new Vector3(0.21f, 1f, 0.21f),
            Vector3.up, Quaternion.identity, Mathf.Infinity, HeroLayer, QueryTriggerInteraction.Collide);

        if (hit && !HeroController.Instance.HasBeenHitDead)
        {
            HeroController.Instance.HasBeenHitDead = true;
        }
    }


    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "RoadOutCycler")
        {
            Destroy(gameObject);
        }
    }
    // void OnDrawGizmos() {
    // 	Gizmos.DrawWireCube(transform.position, new Vector3(0.5f, 20f, 0.5f));
    // }
}
