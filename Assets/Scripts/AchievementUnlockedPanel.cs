﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementUnlockedPanel : MonoBehaviour
{

    public GameObject AchievementTextGameObject;
    public string AchievementText;

    // Use this for initialization
    void Start()
    {
        AchievementTextGameObject.GetComponent<Text>().text = AchievementText;
    }
}
