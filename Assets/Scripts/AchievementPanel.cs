﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementPanel : MonoBehaviour {

	public GameObject ClaimButtonGameObject;
	public GameObject GemDetailGameObject;
	public GameObject GemTextGameObject;
	public GameObject ClaimedTextGameObject;

	public string AchievementName;
	public int GemValue;

	private int IsAchieved;
	private int IsClaimed;

	// Use this for initialization
	void Start () {
		// Debugger
		// PlayerPrefs.SetInt(AchievementName + "Claimed", 0);

		IsAchieved = PlayerPrefs.GetInt(AchievementName + "Achieved", 0);
		IsClaimed = PlayerPrefs.GetInt(AchievementName + "Claimed", 0);

		ClaimedTextGameObject.SetActive(IsClaimed != 0);
		GemDetailGameObject.SetActive(IsClaimed == 0);
		ClaimButtonGameObject.SetActive(IsClaimed == 0 && IsAchieved != 0);

		GemTextGameObject.GetComponent<Text>().text = GemValue.ToString();
	}
	
	public void Claim() {
		GemController.Instance.IncreaseGem(GemValue);

		PlayerPrefs.SetInt(AchievementName + "Claimed", 1);
		IsClaimed = PlayerPrefs.GetInt(AchievementName + "Claimed", 0);

		ClaimedTextGameObject.SetActive(IsClaimed != 0);
		GemDetailGameObject.SetActive(IsClaimed == 0);
		ClaimButtonGameObject.SetActive(IsClaimed == 0 && IsAchieved != 0);
	}
}
