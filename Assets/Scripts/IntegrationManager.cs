﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using System.Threading.Tasks;

public class IntegrationManager : MonoBehaviour {

    public static IntegrationManager Instance;

    [HideInInspector]
    public Student LoggedInStudent;

    [HideInInspector]
    public int Credits;
    public bool HasCredits
    {
        get
        {
            return Credits != 0;
        }
    }
    public GameObject CreditTracker;
    private GameObject creditTrackerInstance;


    // Use this for initialization
    void Start () {
        if (Instance == null)
        {
            Instance = this;
        }
        if (this != Instance)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }
    
    // Update is called once per frame
    public void GetPoints()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://13.250.11.43:8100/api/get-child-points.php?student_id={0}", Instance.LoggedInStudent.studentid));
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        jsonResponse = "{\"values\":" + jsonResponse + "}";
        PointsWrapper info = JsonUtility.FromJson<PointsWrapper>(jsonResponse);
        Instance.Credits = Int32.Parse(info.values[0].credit_points);
    }

    public async void UpdatePoints()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest
            .Create(
                String.Format("http://13.250.11.43:8100/api/update-child-points.php?student_id={0}&credit_points={1}",
                Instance.LoggedInStudent.studentid, Credits)
            );
        HttpWebResponse response =  (HttpWebResponse)(await request.GetResponseAsync());
        StreamReader reader = new StreamReader(response.GetResponseStream());
        reader.ReadToEnd();
    }

    public void StartCrediting()
    {
        creditTrackerInstance = Instantiate(CreditTracker);
    }

    public void StopCrediting()
    {
        if (creditTrackerInstance != null)
        {
            Destroy(creditTrackerInstance);
        }
    }

    public void RemoveCredits()
    {
        if (Credits > 0)
        {
            IntegrationManager.Instance.Credits--;
        }
    }
}
