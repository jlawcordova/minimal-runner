﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMainButton : MonoBehaviour {

	// Use this for initialization
	public void OnBackToMainButtonClicked()
	{
		SceneManager.LoadScene("MainScene");
	}
}
