﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    public static HeroController Instance;

    #region Hero Components
    private Rigidbody rigidBody;
    public LayerMask RoadLayer;
    #endregion

    #region Hero Controls
    private float HorizontalAxis = 0f;
    public float Sensitivity = 1f;
    #endregion

    #region Hero Movement
    public float StartingSpeed = 5f;
    public float HorizontalSpeed = 2f;
    public float CurrentSpeed;
    private float ActualSpeed;
    #endregion

    #region Hero Death
    public bool IsDead = false;
    public bool HasBeenHitDead = false;
    private bool IsEnderShown = false;

    public GameObject[] DeathPointDetectGameObjects;
    public int DeathPointDetectThreshold = 4;
    private List<Vector3> DeathPointsDetected = new List<Vector3>();

    public float DeathFallForcePower = 50f;
    public float DeathFallVerticalSpeed = 10f;

    public float SlowDownDeathTime = 2f;
    private float SlowDownTimer = 0f;
    private float SlowDownDeathTimer = 0f;


    public float TimeScaleOnDeath = 0.25f;
    public float FixedDeltaTimeOnDeath = 0.01f;
    private float TimeScaleBeforeDeath;
    private float FixedDeltaTimeBeforeDeath;
    #endregion

    #region Acceleration Status
    // Used to detect if the hero is being accelerated at an instance.
    // Avoids double accelation for a hero which hits two speed bumps
    // at the same time.
    private bool isBeingAccelerated = false;
    // Lerp timer for temporary acceleration from speedbumps.
    private float accelerationLerpTimer = 0.0f;
    private SpeedBumpValues acceleratorSpeedBumpValues = null;
    #endregion

    #region Hero Upgrades
    public Material[] HeroMaterials;
    #endregion

    public GameObject ParallaxGameObject;
    private FreeParallax parallax;

    public GameObject Canvas;
    public GameObject[] EnderGameObjects;

    // Use this for initialization
    void Start()
    {
        Instance = this;

        rigidBody = GetComponent<Rigidbody>();
        CurrentSpeed = 0.0f;
        // CurrentSpeed = StartingSpeed;

        parallax = ParallaxGameObject.GetComponent<FreeParallax>();

        Renderer meshRenderer = GetComponent<Renderer>();
        meshRenderer.material = HeroMaterials[SelectedHeroController.Instance.SelectedHero];
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDead && HasBeenHitDead)
        {
            HorizontalAxis = 0;
            return;
        }

#if UNITY_EDITOR
        // Get movement from player input
        HorizontalAxis = Input.GetAxis("Horizontal");
#endif

#if UNITY_ANDROID
        float target;
        if (Input.touchCount > 0)
        {

            var touchPosition = Input.GetTouch(0).position;
            if (touchPosition.x > Screen.width / 2)
            {
                target = 1;
            }
            else
            {
                target = -1;
            }

            HorizontalAxis = Mathf.MoveTowards(HorizontalAxis, target, Sensitivity * Time.deltaTime);
        }
        else
        {
            target = 0;
        }
        HorizontalAxis = Mathf.MoveTowards(HorizontalAxis, target, Sensitivity * Time.deltaTime);
    }
#endif

    void FixedUpdate()
    {
        parallax.Speed = HorizontalAxis * 2;

        if (GameManager.Instance.HasStarted)
        {
            Move();
            CheckDeath();
        }
    }

    private void Move()
    {
        ActualSpeed = CurrentSpeed;
        if (!IsDead)
        {
            ActualSpeed += GetSpeedbumpAccelerationBonus();
        }

        var velocity = new Vector3();
        velocity.x = HorizontalAxis * HorizontalSpeed;
        velocity.y = rigidBody.velocity.y;
        velocity.z = ActualSpeed;

        rigidBody.velocity = velocity;
    }

    private void CheckDeath()
    {
        if (!IsDead)
        {
            if (!HasBeenHitDead)
            {
                GetDeathPointDetects();

                if (DeathPointsDetected.Count >= DeathPointDetectThreshold)
                {
                    SoundEffectPlayer.Instance.PlayDeath();
                    Die();
                    SetupSlowDown();
                    FallToDeath();
                }
            }
            else
            {
                SoundEffectPlayer.Instance.PlayDeath();
                Die();
                SetupSlowDown();
            }

        }
        else
        {
            SlowDown();
            SlowDownToDeath();
        }
    }

    private void SlowDown()
    {
        if (SlowDownTimer < SlowDownDeathTime)
        {
            SlowDownTimer += Time.deltaTime;
            float t = SlowDownTimer / SlowDownDeathTime;
            t = Interpolation.EaseOut(t);

            CurrentSpeed = Mathf.Lerp(
                CurrentSpeed,
                0,
                t
            );
        }
    }

    private void GetDeathPointDetects()
    {
        DeathPointsDetected.Clear();

        foreach (var fallDetectGameObject in DeathPointDetectGameObjects)
        {
            bool isFall = !Physics.Raycast(fallDetectGameObject.transform.position,
                Vector3.down, 10f, RoadLayer, QueryTriggerInteraction.Collide);
            if (isFall)
            {
                DeathPointsDetected.Add(fallDetectGameObject.transform.localPosition);
            }
        }
    }

    private void Die()
    {
        IntegrationManager.Instance.StopCrediting();
        IntegrationManager.Instance.UpdatePoints();
        IsDead = true;
    }

    public void SetupSlowDown()
    {
        TimeScaleBeforeDeath = Time.timeScale;
        FixedDeltaTimeBeforeDeath = Time.fixedDeltaTime;
    }

    private void FallToDeath()
    {
        // Make the hero spin upon falling.
        rigidBody.freezeRotation = false;
        foreach (var pointOfFall in DeathPointsDetected)
        {
            rigidBody.AddForceAtPosition(
                Vector3.up * ActualSpeed * DeathFallForcePower,
                pointOfFall);
            rigidBody.AddForceAtPosition(
                Vector3.left * ActualSpeed * DeathFallForcePower,
                pointOfFall);
        }
        rigidBody.velocity = new Vector3(
            rigidBody.velocity.x,
            -DeathFallVerticalSpeed,
            rigidBody.velocity.z
        );
    }

    private void SlowDownToDeath()
    {
        if (SlowDownDeathTimer < SlowDownDeathTime)
        {
            SlowDownDeathTimer += Time.deltaTime;
            float t = SlowDownDeathTimer / SlowDownDeathTime;
            t = Interpolation.EaseOut(t);

            Time.timeScale = Mathf.Lerp(
                TimeScaleOnDeath,
                TimeScaleBeforeDeath,
                t
            );

            Time.fixedDeltaTime = Mathf.Lerp(
                FixedDeltaTimeOnDeath,
                FixedDeltaTimeBeforeDeath,
                t
            );
        }
        else
        {
            if (!IsEnderShown)
            {
                ShowHideEnderGameObjects();
                IsEnderShown = true;
            }
            // GameManager.Reset();
        }
    }

    private void ShowHideEnderGameObjects()
    {
        foreach (GameObject enderGameObject in EnderGameObjects)
        {
            Instantiate(
                enderGameObject,
                Canvas.transform
            );
        }

        foreach (GameObject gameModeUIGameObjectInstance in GameManager.Instance.GameModeUIGameObjectInstances)
        {
            Animator animator = gameModeUIGameObjectInstance.GetComponent<Animator>();
            animator.SetBool("shouldOut", true);
        }
    }

    private float GetSpeedbumpAccelerationBonus()
    {
        if (acceleratorSpeedBumpValues != null
            && accelerationLerpTimer < acceleratorSpeedBumpValues.AccelerationLerpTime)
        {
            accelerationLerpTimer += Time.deltaTime;
            float t = accelerationLerpTimer / acceleratorSpeedBumpValues.AccelerationLerpTime;
            t = Interpolation.EaseOut(t);
            return Mathf.Lerp(
                acceleratorSpeedBumpValues.AccelarationTemporaryAddition,
                0f,
                t);
        }

        return 0f;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Speedbump" && !isBeingAccelerated)
        {
            Speedbump speedBump = collider.gameObject.GetComponent<Speedbump>();
            Accelerate(speedBump);
        }
    }

    void Accelerate(Speedbump speedBump)
    {
        isBeingAccelerated = true;
        accelerationLerpTimer = 0f;

        CurrentSpeed += speedBump.SpeedupAddition;
        acceleratorSpeedBumpValues = new SpeedBumpValues()
        {
            AccelarationTemporaryAddition = speedBump.AccelarationTemporaryAddition,
            AccelerationLerpTime = speedBump.AccelerationLerpTime
        };
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Speedbump")
        {
            ToggleAcceleration();
        }
    }

    private void ToggleAcceleration()
    {
        isBeingAccelerated = false;
    }
}

class SpeedBumpValues
{
    public float AccelarationTemporaryAddition;
    public float AccelerationLerpTime;
}
