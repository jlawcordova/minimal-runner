﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadController : MonoBehaviour
{

    public GameObject[] RoadGameObjects;
    public GameObject[] HealthyFoodGameObjects;
    public GameObject[] JunkFoodGameObjects;
    public GameObject GemGameObject;

    public float FoodChance = 0.55f;
    public float GemChance = 0.05f;
    public float HealthyFoodChance = 0.4f;
    public float HealthyFoodRowChance = 0.35f;

    public int MaxCurrentRoads = 10;
    public Vector3 StartPosition = new Vector3(0f, 0f, -20f);
    public static float RoadSpeed = 5f;

    private Vector3 positionToPlace;

    // Use this for initialization
    public void Start()
    {
        Queue<GameObject> initialRoads;
        FillInitialRoads(out initialRoads);
        SetupInitialRoads(initialRoads);
    }

    public void FillInitialRoads(out Queue<GameObject> initialRoads)
    {
        initialRoads = new Queue<GameObject>();
        // Ensure first two roads are straight.
        initialRoads.Enqueue(RoadGameObjects[0]);
        initialRoads.Enqueue(RoadGameObjects[0]);
        
        for (int i = 2; i < MaxCurrentRoads; i++)
        {
            int randomRoadIndex = UnityEngine.Random.Range(0, RoadGameObjects.Length);
            initialRoads.Enqueue(RoadGameObjects[randomRoadIndex]);
        }
    }

    private void SetupInitialRoads(Queue<GameObject> initialRoads)
    {
        positionToPlace = StartPosition;

        while (initialRoads.Count != 0)
        {
            GameObject currentRoadGameObject = initialRoads.Dequeue();

            InstantiateRoad(currentRoadGameObject);
        }
    }

    private void InstantiateRoad(GameObject currentRoadGameObject)
    {
        GameObject placedCurrentRoadGameObject;

        placedCurrentRoadGameObject = Instantiate(currentRoadGameObject, positionToPlace, Quaternion.identity);

        InstantiateFood(positionToPlace, placedCurrentRoadGameObject.transform);

        placedCurrentRoadGameObject.GetComponent<Road>()
            .OnRoadCycledOut += CycleInRoad;
        positionToPlace = placedCurrentRoadGameObject
            .GetComponentsInChildren<Transform>()
            .Where(t => t.tag == "Endpoint")
            .First()
            .transform.position;
    }

    private void InstantiateFood(Vector3 positionToPlace, Transform parent)
    {
        var healthyFoodGameObjectIndex = UnityEngine.Random.Range(0, HealthyFoodGameObjects.Length);
        var junkFoodGameObjectIndex = UnityEngine.Random.Range(0, JunkFoodGameObjects.Length);

        if (UnityEngine.Random.value < FoodChance)
        {
            float xOffset = -1.5f;
            var offsetChance = UnityEngine.Random.value;
            if (offsetChance < 0.33f)
            {
                xOffset = 0f;
            }
            if (offsetChance > 0.66f)
            {
                xOffset = 1.5f;
            }

            var isGem = UnityEngine.Random.value < GemChance;
            var isHealthy = UnityEngine.Random.value < HealthyFoodChance;

            if (isGem)
            {
                Vector3 position = new Vector3(
                    positionToPlace.x + xOffset,
                    positionToPlace.y,
                    positionToPlace.z
                );
                Instantiate(GemGameObject, position, Quaternion.identity);
                return;
            }

            if (isHealthy)
            {
                if (UnityEngine.Random.value < HealthyFoodRowChance)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        Vector3 position = new Vector3(
                            positionToPlace.x + xOffset,
                            positionToPlace.y,
                            positionToPlace.z + (4f * i)
                        );
                        Instantiate(HealthyFoodGameObjects[healthyFoodGameObjectIndex], position, Quaternion.identity);
                    }
                }
                else
                {
                    Vector3 position = new Vector3(
                        positionToPlace.x + xOffset,
                        positionToPlace.y,
                        positionToPlace.z
                    );
                    Instantiate(HealthyFoodGameObjects[healthyFoodGameObjectIndex], position, Quaternion.identity);
                }
            }
            else
            {
                Vector3 position = new Vector3(
                        positionToPlace.x + xOffset,
                        positionToPlace.y,
                        positionToPlace.z
                    );
                Instantiate(JunkFoodGameObjects[junkFoodGameObjectIndex], position, Quaternion.identity);
            }
        }
    }

    private void CycleInRoad(object sender, EventArgs e)
    {
        int randomRoadIndex = UnityEngine.Random.Range(0, RoadGameObjects.Length);
        InstantiateRoad(RoadGameObjects[randomRoadIndex]);
    }
}
