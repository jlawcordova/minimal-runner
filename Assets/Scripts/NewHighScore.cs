﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewHighScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (!GameManager.Instance.HasNewHiScore)
		{
			Destroy(gameObject);
		}
	}
}
