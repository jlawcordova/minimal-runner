﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemController : MonoBehaviour {

    public static GemController Instance;
    public int Gem = 0;
	// Use this for initialization
	void Start () {
		if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(gameObject);
        }

		DontDestroyOnLoad(gameObject);
        Gem = PlayerPrefs.GetInt("Gem", 0);
	}
	
	// Update is called once per frame
	public void IncreaseGem(int gem)
    {
        Gem += gem;
        PlayerPrefs.SetInt("Gem", Gem);
    }

	public void DecreaseGem(int gem)
    {
        Gem -= gem;
        PlayerPrefs.SetInt("Gem", Gem);
    }
}
