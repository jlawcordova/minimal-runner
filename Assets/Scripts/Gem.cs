﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour {

	public LayerMask HeroLayer;
    private int gem = 1;
    private bool scored = false;

    public GameObject HealthyFoodContainer;
    private Animator healthyFoodContainerAnimator;

    // Use this for initialization
    void Start()
    {
        healthyFoodContainerAnimator = HealthyFoodContainer.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        var hit = Physics.BoxCast(transform.position, new Vector3(0.21f, 1f, 0.21f),
            Vector3.up, Quaternion.identity, Mathf.Infinity, HeroLayer, QueryTriggerInteraction.Collide);

        if (hit && !scored)
        {
            healthyFoodContainerAnimator.SetTrigger("shouldFly");
            scored = true;
            SoundEffectPlayer.Instance.PlayGem();
            GemAchievementTracker.Instance.IncreaseGemsAcquired();
            GemController.Instance.IncreaseGem(gem);
            Invoke("DelayedDestory", 0.5f);
        }
    }

    public void DelayedDestory()
    {
        Destroy(gameObject);
    }


    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "RoadOutCycler")
        {
            Destroy(gameObject);
        }
    }
}
