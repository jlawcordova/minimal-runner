﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiScoreText : MonoBehaviour {

	// Use this for initialization
	public Text Text;

	// Use this for initialization
	void Start () {
		Text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		Text.text = GameManager.Instance.HiScore.ToString();
	}
}
