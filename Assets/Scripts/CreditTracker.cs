﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditTracker : MonoBehaviour {

    public int RemoveCreditSeconds = 3;

    private int ExtraSeconds;

    // Use this for initialization
    void Start () {
        ExtraSeconds = PlayerPrefs.GetInt("ExtraSeconds", 0);
        InvokeRepeating("RemoveCredit", ExtraSeconds, RemoveCreditSeconds);
        InvokeRepeating("RemoveExtraSecond", 0, 1);
    }
    
    void RemoveCredit () {
        IntegrationManager.Instance.RemoveCredits();
        ExtraSeconds = RemoveCreditSeconds;
    }

    void RemoveExtraSecond () {
        ExtraSeconds--;
        PlayerPrefs.SetInt("ExtraSeconds", ExtraSeconds);
    }
}