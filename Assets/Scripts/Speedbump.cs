﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedbump : MonoBehaviour
{
    public float SpeedupAddition = 3f;
    public float AccelarationTemporaryAddition = 50f;
    public float AccelerationLerpTime = 3f;
}
