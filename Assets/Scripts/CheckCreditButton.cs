﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCreditButton : MonoBehaviour {

    public GameObject[] RemoveWithCreditGameObjects;

    void Update()
    {
        if (IntegrationManager.Instance.Credits > 0)
        {
            foreach (var removeWithCreditGameObject in RemoveWithCreditGameObjects)
            {
                Destroy(removeWithCreditGameObject);
            }
        }
    }

    public void OnCheckCreditButtonClicked()
    {
        IntegrationManager.Instance.GetPoints();
        ZeroCreditSpawner.Instance.HasSpawned = false;
        Time.timeScale = 1;
    }
}
