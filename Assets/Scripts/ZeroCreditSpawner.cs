﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroCreditSpawner : MonoBehaviour {

    public static ZeroCreditSpawner Instance;

    public GameObject Canvas;
    public GameObject MainCamera;
    public GameObject MaskGameObject;
    public GameObject[] SpawnGameObjects;

    public bool HasSpawned = false;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Update ()
    {
        if (!HasSpawned)
        {
            if(IntegrationManager.Instance == null)
            {
                return;
            }

            if(IntegrationManager.Instance.Credits == 0)
            {
                IntegrationManager.Instance.UpdatePoints();
                foreach (GameObject spawnGameObject in SpawnGameObjects)
                {
                    Instantiate(spawnGameObject, Canvas.transform);
                }
                Instantiate(MaskGameObject, MainCamera.transform);
                HasSpawned = true;
                Time.timeScale = 0;
            }
        }
    }
}
