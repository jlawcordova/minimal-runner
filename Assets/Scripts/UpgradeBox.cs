﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeBox : MonoBehaviour
{

    public int UpgradeBoxIndex;
    public int UpgradeCost;
    public GameObject UpgradeCostTextGameObject;
    public GameObject SelectedHeroTextGameObject;
    public GameObject[] LockedGameObjects;
    public GameObject[] UnlockedGameObjects;

    void Start()
    {
        UpgradeCostTextGameObject.GetComponent<Text>().text = UpgradeCost.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject lockedGameObject in LockedGameObjects)
        {
            lockedGameObject.SetActive(!UpgradeController.Instance.UpgradeUnlocked[UpgradeBoxIndex]);
        }
        foreach (GameObject unlockedGameObject in UnlockedGameObjects)
        {
            unlockedGameObject.SetActive(UpgradeController.Instance.UpgradeUnlocked[UpgradeBoxIndex]);
        }
        SelectedHeroTextGameObject.SetActive(SelectedHeroController.Instance.SelectedHero == UpgradeBoxIndex);
    }

    public void OnUpgradeBoxClicked()
    {
        if(UpgradeController.Instance.UpgradeUnlocked[UpgradeBoxIndex])
        {
            SelectedHeroController.Instance.SetSelectedHero(UpgradeBoxIndex);
        }
        else
        {
            if (GemController.Instance.Gem >= UpgradeCost)
            {
                // TODO: Show dialog.
                ConfirmationPanel.Instance.AffectedUpgradeBoxIndex = UpgradeBoxIndex;
                ConfirmationPanel.Instance.AffectedUpgradeBoxCost = UpgradeCost;
                ConfirmationPanel.Instance.Show();
            }
        }
    }
}
