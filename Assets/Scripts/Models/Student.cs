using System;

[Serializable]
public class Student
{
    public string studentid;
    public string firstname;
    public string middlename;
    public string lastname;
}

[Serializable]
public class StudentWrapper
{
    public Student[] values;
}