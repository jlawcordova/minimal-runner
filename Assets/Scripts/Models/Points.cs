using System;

[Serializable]
public class Points
{
    public string student_id;
    public string credit_points;
}

[Serializable]
public class PointsWrapper
{
    public Points[] values;
}