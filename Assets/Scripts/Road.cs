﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Road : MonoBehaviour
{
    public event EventHandler OnRoadCycledOut;

    private GameObject endpointGameObject;

    void Start()
    {
    }

    public void CycleOut()
    {
        InvokeOnRoadCycledOut();
        Destroy(gameObject);
    }

    private void InvokeOnRoadCycledOut()
    {
        if (OnRoadCycledOut != null)
        {
            OnRoadCycledOut.Invoke(this, null);
        }
    }

    void FixedUpdate()
    {
    }

    public class OnRoadCycledOutDelegate
    {
    }
}
