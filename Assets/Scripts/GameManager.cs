﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int Score = 0;

    public GameObject[] StartGameObjects;
    public List<GameObject> GameModeUIGameObjectInstances;
    public GameObject[] GameModeUIGameObjects;
    public GameObject Canvas;
    public GameObject Hero;

    public bool HasStarted = false;

    private HeroController heroController;
    public int HiScore;
    public bool HasNewHiScore;

    // Use this for initialization
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(this);
        }

        HasNewHiScore = false;
        Score = 0;
        HiScore = PlayerPrefs.GetInt("HiScore", 0);
        GameModeUIGameObjectInstances = new List<GameObject>();
        heroController = Hero.GetComponent<HeroController>();

        IntegrationManager.Instance.GetPoints();
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (!HasStarted && Input.GetMouseButtonDown(0))
        {
            StartGame();
        }
#endif

#if UNITY_IPHONE
        if (!HasStarted && Input.touchCount > 0)
        {
            StartGame();
        }
#endif

#if UNITY_ANDROID
        if (!HasStarted && Input.touchCount > 0)
        {
            StartGame();
        }
#endif
    }

    private void StartGame()
    {
        heroController.CurrentSpeed = heroController.StartingSpeed;
        HasStarted = true;
        IntegrationManager.Instance.StartCrediting();
        HideStartGameObjects();
    }

    private void HideStartGameObjects()
    {
        foreach (GameObject item in StartGameObjects)
        {
            Animator animator = item.GetComponent<Animator>();
            animator.SetBool("shouldOut", true);
        }
        foreach (GameObject gameModeUIGameObject in GameModeUIGameObjects)
        {
            GameModeUIGameObjectInstances.Add(Instantiate(gameModeUIGameObject, Canvas.transform));
        }
    }

    public void IncreaseScore(int score)
    {
        Score += score;
        ScoreAchievementTracker.Instance.CheckScoreAchievement(Score);
        if (Score > HiScore)
        {
            HiScore = Score;
            HasNewHiScore = true;
            PlayerPrefs.SetInt("HiScore", Score);
        }
    }

    public void Reset()
    {
        Score = 0;
        SceneManager.LoadScene("MainScene");
    }
}
